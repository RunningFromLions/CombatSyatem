﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Stats : MonoBehaviour
{
    public CreateNewCharacter Toon_Profile;
    public Dictionary<string, Attributes.Values> Attributes = new Dictionary<string, global::Attributes.Values>();
    public class Collections
    {
        public int Experience;
        public int ExperienceToLevel;
        public int ExperienceOnKill;
        public float ExperienceMultiplier;

        public int Gold;
        public int GoldOnKill;
        public int GoldMultiplier;

        public int Kills;
        public int Deaths;

        public int DamageDealt;
        public int DamageTaken;

        public int HealingDone;
        public int HealingTaken;

        public List<CreateNewCharacter> CreaturesKilled = new List<CreateNewCharacter>();
        public List<int> TimesKilled = new List<int>();

        public int KillStreak;
        public int DeathStreak;

        public GameObject LastAttackedBy;
    }
    public Collections Collection;
    public CreateNewFaction Faction;
    public List<string> RegenStats = new List<string>();

    void Awake()
    {
        Collection = new Collections();

        for(int i = 0; i < Toon_Profile.Stats.Count; i++)
        {
            Attributes.Add(Toon_Profile.Stats[i].Stat.ToString(), Toon_Profile.Stats[i].Value);
        }

        RegenStats = GetRegenStats();
    }

    #region regeneration process
    List<string> GetRegenStats()
    {
        List<string> Values = new List<string>();
        #region check which stats we need to start a regen process for, then start the regen process
        foreach (System.Collections.Generic.KeyValuePair<string, Attributes.Values> key in Attributes)
        {
            if(key.Value.Regen > 0)
            {
                Values.Add(key.Key);
            }
        }
        return Values;
        #endregion
    }

    void DoRegen(List<string> Stats, float Interval)
    {
        #region check if regen amount is less that Max - Current, if it is, add the regen amount
        for(int i = 0; i < Stats.Count; i++)
        {
            int Amount = Attributes[Stats[i]].Regen;
            int Current = Attributes[Stats[i]].Current;
            int Max = Attributes[Stats[i]].Max;
            int diff = Max - Current;

            if(Amount < diff)
            {
                Attributes[Stats[i]].Current += Amount;
            }
            else
            {
                Attributes[Stats[i]].Current = Max;
            }
        }
        #endregion 
    }

    float interval = 5;
    float T = 1;
    float elapsed;
    void Update()
    {
        elapsed += Time.deltaTime;
        if(elapsed >= interval * T)
        {
            T++;
            DoRegen(RegenStats, interval);
        }
    }
    #endregion
}
