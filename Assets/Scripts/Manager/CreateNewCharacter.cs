﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

//creates a new scriptable object for Buffs

[CreateAssetMenu(fileName = "New Character", menuName = "New Character", order = 1)]
[System.Serializable]
public class CreateNewCharacter : ScriptableObject
{
    public string ToonID;
    public string Toon_Name;
    public string Toon_Description;
    public Sprite Icon;
    public GameObject Model;
    public Vector3 SpellOrigin;
    public Vector3 SpellDestination;

    public List<Attributes> Stats = new List<Attributes>();
    public List<Attributes.StatName> StatsIndex= new List<Attributes.StatName>();

    public CreateNewDamageType Type;
    public CreateNewResource ResourceType;

    public List<SpellObject> Skills = new List<SpellObject>();

    [System.Serializable]
    public class CharacterAudio
    {
        public enum ClipName
        {
            Slected,
            Attack,
            Death,
            Error
        }

        public ClipName Name;
        public AudioClip Clip;
    }
    public List<CharacterAudio> AudioClips = new List<CharacterAudio>();

    [System.Serializable]
    public class LevelUp
    {
        public AnimationCurve XPCurve;
        public CreateNewCharacter PreviousEvolution;
        public CreateNewCharacter NextEvolution;
        public int NextEvolutionLevel;
    }
    public LevelUp LevelInfo;
}
