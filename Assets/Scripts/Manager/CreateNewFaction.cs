﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;


[CreateAssetMenu(fileName = "New Faction", menuName = "New Faction", order = 1)]
[System.Serializable]
public class CreateNewFaction : ScriptableObject{

    public enum Names
    {
        None,
        Red,
        Green,
        Blue,
        Yellow
    }

    public Names Name;
    public List<Names> FriendList = new List<Names>();
    public List<Names> NeutralList = new List<Names>();
    public List<Names> FoeList = new List<Names>();
}
