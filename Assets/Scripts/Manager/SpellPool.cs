﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SpellPool : MonoBehaviour {

    public int SpellPoolSize;
    public int ActiveInstances;
    public int AvailableInstances;
    public SpellObject[] AllSpells;
    public List<SpellInstance> Pool = new List<SpellInstance>();
    SpellInstance EmptyInstance;

	// Use this for initialization
	void Start() {
        AllSpells = Resources.LoadAll<SpellObject>("Spells").OrderBy(x => x.name).ToArray();
        EmptyInstance = new SpellInstance();
        SetUpPool();
    }

    /// <summary>
    /// initialize the Spell Pool
    /// </summary>
    public void SetUpPool()
    {
        for (int i = 0; i < SpellPoolSize; i++)
        {
            SpellInstance S = new SpellInstance();
            Pool.Add(S);
            AvailableInstances++;
        }
    }

    /// <summary>
    /// Get an Instance from the pool
    /// </summary>
    /// <param name="O"></param>
    /// <returns></returns>
    public SpellInstance FetchInstance(SpellObject O)
    {
        if(Pool.Count > 0)
        {
            SpellInstance I = Pool[0];
            Pool.RemoveAt(0);
            ActiveInstances++;
            AvailableInstances--;
            CopyInstance(I, O);
            TimerSetUp(I);
            return I;
        }
        else
        {
            Debug.Log("No Available Instances, Ask a coder to increase the Spell Pool Size");
            return null;
        }
    }

    /// <summary>
    /// Return instance to the Spell Pool
    /// </summary>
    /// <param name="S"></param>
    public void ReturnInstance(SpellInstance S)
    {
        ResetInstance(S, EmptyInstance);
        Pool.Add(S);
        ActiveInstances--;
        AvailableInstances++;
    }

    /// <summary>
    /// Copy Values from Spell Object to Spell Instance
    /// </summary>
    /// <param name="I"></param>
    /// <param name="O"></param>
    public void CopyInstance(SpellInstance I, SpellObject O)
    {
        I.SType = O.SType;
        I.SCategory = O.SCategory;
        I.SpellID = O.SpellID;
        I.SpellName = O.SpellName;
        I.ToolTip = O.ToolTip;
        I.Icon = O.Icon;

        I.Stackable = O.Stackable;
        I.StackLimit = O.StackLimit;
        I.CurrentStacks = O.CurrentStacks;

        I.UseParticles = O.UseParticles;
        I.Particles = O.Particles;

        I.Hostile = O.Hostile;
        I.Friendly = O.Friendly;
        I.Neutral = O.Neutral;

        I.CastTime = O.CastTime;
        I.CastWhileMoving = O.CastWhileMoving;
        I.Channel = O.Channel;
        I.CoolDown = O.CoolDown;
        I.CD_Remaining = O.CD_Remaining;
        I.Ready = O.Ready;

        I.MaxRange = O.MaxRange;
        I.MinRange = O.MinRange;

        I.Resource_Cost = O.Resource_Cost;
        I.Resource_Type = O.Resource_Type;

        I.Source = O.Source;
        I.Target = O.Target;

        I.Source_PowerOnCast = O.Source_PowerOnCast;

        I.ToatalDamage = O.ToatalDamage;
        I.CurrentDamage = O.CurrentDamage;
        I.Duration = O.Duration;
        I.UnlimitedDuration = O.UnlimitedDuration;
        I.UTickLength = O.UTickLength;
        I.Ticks = O.Ticks;
        I.Applied = O.Applied;
        I.Behavior = O.Behavior;

        I.Attributes = O.Attributes;
        I.Multipliers = O.Multipliers;

        I.Path = O.Path;
        I.TravelTime = O.TravelTime;
        I.TravelBehavior = O.TravelBehavior;

        I.Begin = O.Begin;
        I.End = O.End;

        I.Shield_MaxHealth = O.Shield_MaxHealth;
        I.Shield_CurrentHealth = O.Shield_CurrentHealth;
        I.Specific_Type = O.Specific_Type;
        I.ShieldType = O.ShieldType;
        I.SpellOnBreak = O.SpellOnBreak;
        I.BreakEffects = O.BreakEffects;
        I.Shield_Duration = O.Shield_Duration;

        I.ApplyEffect = O.ApplyEffect;
        I.SecondaryTarget = O.SecondaryTarget;
        I.SecondarySpells = O.SecondarySpells;

        I.DamageType = O.DamageType;

        I.Req = O.Req;
        I.Require = O.Require;

        I.isAOE = O.isAOE;
        I.AOEstyle = O.AOEstyle;
        I.ActiveDuringTravel = O.ActiveDuringTravel;
        I.Radius = O.Radius;
        I.Shape = O.Shape;
        I.MaxEnemiesEffected = O.MaxEnemiesEffected;

        I.SpellSounds = O.SpellSounds;
    }

    /// <summary>
    /// set up the timer on the instance;
    /// </summary>
    /// <param name="I"></param>
    public void TimerSetUp(SpellInstance I)
    {
        I.SpellTimer = new Timer();
        I.SpellTimer.Spell = I;
    }

    /// <summary>
    /// Resets the spell instance to an empty state.
    /// </summary>
    /// <param name="I"></param>
    /// <param name="O"></param>
    public void ResetInstance(SpellInstance I, SpellInstance O)
    {
        I.SType = O.SType;
        I.SCategory = O.SCategory;
        I.SpellID = O.SpellID;
        I.SpellName = O.SpellName;
        I.ToolTip = O.ToolTip;
        I.Icon = O.Icon;

        I.Stackable = O.Stackable;
        I.StackLimit = O.StackLimit;
        I.CurrentStacks = O.CurrentStacks;

        I.UseParticles = O.UseParticles;
        I.Particles = O.Particles;

        I.Hostile = O.Hostile;
        I.Friendly = O.Friendly;
        I.Neutral = O.Neutral;

        I.CastTime = O.CastTime;
        I.CastWhileMoving = O.CastWhileMoving;
        I.Channel = O.Channel;
        I.CoolDown = O.CoolDown;
        I.CD_Remaining = O.CD_Remaining;
        I.Ready = O.Ready;

        I.MaxRange = O.MaxRange;
        I.MinRange = O.MinRange;

        I.Resource_Cost = O.Resource_Cost;
        I.Resource_Type = O.Resource_Type;

        I.Source = O.Source;
        I.Target = O.Target;

        I.Source_PowerOnCast = O.Source_PowerOnCast;

        I.ToatalDamage = O.ToatalDamage;
        I.CurrentDamage = O.CurrentDamage;
        I.Duration = O.Duration;
        I.UnlimitedDuration = O.UnlimitedDuration;
        I.UTickLength = O.UTickLength;
        I.Ticks = O.Ticks;
        I.Applied = O.Applied;
        I.Behavior = O.Behavior;
        I.SpellTimer = O.SpellTimer;
        TimerSetUp(I);

        I.Attributes = O.Attributes;
        I.Multipliers = O.Multipliers;

        I.Path = O.Path;
        I.TravelTime = O.TravelTime;
        I.TravelBehavior = O.TravelBehavior;

        I.Begin = O.Begin;
        I.End = O.End;

        I.Shield_MaxHealth = O.Shield_MaxHealth;
        I.Shield_CurrentHealth = O.Shield_CurrentHealth;
        I.Specific_Type = O.Specific_Type;
        I.ShieldType = O.ShieldType;
        I.SpellOnBreak = O.SpellOnBreak;
        I.BreakEffects = O.BreakEffects;
        I.Shield_Duration = O.Shield_Duration;

        I.ApplyEffect = O.ApplyEffect;
        I.SecondaryTarget = O.SecondaryTarget;
        I.SecondarySpells = O.SecondarySpells;

        I.DamageType = O.DamageType;

        I.Req = O.Req;
        I.Require = O.Require;

        I.isAOE = O.isAOE;
        I.AOEstyle = O.AOEstyle;
        I.ActiveDuringTravel = O.ActiveDuringTravel;
        I.Radius = O.Radius;
        I.Shape = O.Shape;
        I.MaxEnemiesEffected = O.MaxEnemiesEffected;

        I.SpellSounds = O.SpellSounds;
    }
	
}
