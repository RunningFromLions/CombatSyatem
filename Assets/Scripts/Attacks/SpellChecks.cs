﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpellChecks {

    /// <summary>
    /// returns true if checked object is an enemy or neutral
    /// </summary>
    /// <param name="Spell"></param>
    /// <param name="Player"></param>
    /// <param name="Target"></param>
    /// <returns></returns>
    public static bool AttackableCheck(SpellInstance Spell, GameObject Player, GameObject Target)
    {
        bool isAttackable = false;
        CreateNewFaction P = Player.GetComponent<Stats>().Faction;
        CreateNewFaction T = Target.GetComponent<Stats>().Faction;


        for(int i = 0; i < P.FoeList.Count; i++)
        {
            if(P.FoeList[i] == T.Name)
            {
                isAttackable = true;
            }
            
        }

        return isAttackable;
    }

    /// <summary>
    /// returns true if target is in line of sight of the caster
    /// </summary>
    /// <param name="Source"></param>
    /// <param name="Target"></param>
    /// <returns></returns>
    public static bool LineOfSightCheck(GameObject Source, GameObject Target)
    {
        bool HasLOS = false;

        Vector3 Direction = Target.transform.position - Source.transform.position;
        float Distance = SpellChecks.DistanceCheck(Source, Target);
        RaycastHit hit;

        if (Physics.Raycast(Source.transform.position, Direction, out hit, Distance))
        {
            if (hit.collider.gameObject == Target)
            {
                HasLOS = true;
            }
        }
        else
        {
            HasLOS = false;
        }

        return HasLOS;
    }

    /// <summary>
    /// returns distance from caster to target
    /// </summary>
    /// <param name="Source"></param>
    /// <param name="Target"></param>
    /// <returns></returns>
    public static float DistanceCheck(GameObject Source, GameObject Target)
    {
        float Distance = Vector3.Distance(Source.transform.position, Target.transform.position);
        return Distance;
    }

    /// <summary>
    /// returns true if caster has enough of resource type to cast ability
    /// </summary>
    /// <param name="Caster"></param>
    /// <param name="Spell"></param>
    /// <returns></returns>
    public static bool ResourceCheck(GameObject Caster, SpellInstance Spell)
    {
        bool HasEnough = false;

        Stats CasterStats = Caster.GetComponent<Stats>();
        float Required = Spell.Resource_Cost;
        float Available = CasterStats.Attributes["Resources"].Current;

        if(Available >= Required)
        {
            HasEnough = true;
        }

        return HasEnough;
    }

    public static void StartCastTimer(SpellInstance Spell)
    {
        
    }




}
