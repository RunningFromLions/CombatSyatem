﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ActiveEffects : MonoBehaviour {

    public List<SpellInstance> EffectList = new List<SpellInstance>();
    GameObject Manager;
    SpellPool Pool;

    void Start()
    {
        Manager = GameObject.Find("Manager");
        Pool = Manager.GetComponent<SpellPool>();
    }

    /// <summary>
    /// subscribe the effect list script to the spell timer of the effect and add it to the list
    /// </summary>
    /// <param name="Spell"></param>
    public void AddToList(SpellInstance Spell)
    {
        Spell.SpellTimer.Tick += Tick;
        Spell.SpellTimer.End += End;
        EffectList.Add(Spell);
        Spell.SpellTimer.Cast(Spell);
    }

    /// <summary>
    /// un-subscribe the effect list script from the spell timer of the effect and remove it from the list
    /// </summary>
    /// <param name="Spell"></param>
    public void RemoveFromList(SpellInstance Spell)
    {
        Spell.SpellTimer.Tick -= Tick;
        Spell.SpellTimer.End -= End;
        EffectList.Remove(Spell);
        Pool.ReturnInstance(Spell);
    }

    public void Tick(SpellInstance Spell)
    {
        //Debug.Log("Tick");
    }

    public void End(SpellInstance Spell)
    {
        //Debug.Log("End");
        RemoveFromList(Spell);
    }

    void OnDisable()
    {

    }
}
