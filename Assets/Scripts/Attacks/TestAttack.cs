﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;

public class TestAttack : MonoBehaviour {

    public SpellObject Spell;
    public GameObject Player;
    public GameObject Target;
    public SpellInstance I;
    public GameObject Manager;

	// Use this for initialization
	void Start () {

        //Timing.RunCoroutine(Setup());

	}
	
    IEnumerator<float> Setup()
    {
        SpellPool Pool = Manager.GetComponent<SpellPool>();

        while(Pool.AvailableInstances < Pool.SpellPoolSize)
        {
            Debug.Log("Waiting for pool to initialize");
            yield return 0f;
        }

        I = Pool.FetchInstance(Spell);
        I.Source = Player;
        I.Target = Target;
        I.Source_PowerOnCast = Player.GetComponent<Stats>().Attributes["Power"].Current;
        ActiveEffects A = Target.GetComponent<ActiveEffects>();
        A.AddToList(I);
    }

    void DoSpell()
    {
        SpellPool Pool = Manager.GetComponent<SpellPool>();
        I = Pool.FetchInstance(Spell);
        I.Source = Player;
        I.Target = Target;
        I.Source_PowerOnCast = Player.GetComponent<Stats>().Attributes["Power"].Current;

        if(SpellChecks.AttackableCheck(I, Player, Target))
        {
            if(SpellChecks.ResourceCheck(Player, I))
            {
                if(SpellChecks.DistanceCheck(Player, Target) <= I.MaxRange && SpellChecks.DistanceCheck(Player, Target) >= I.MinRange)
                {
                    if(SpellChecks.LineOfSightCheck(Player, Target))
                    {
                        ActiveEffects A = Target.GetComponent<ActiveEffects>();
                        A.AddToList(I);
                    }
                    else
                    {
                        Debug.Log("Not In Line of Sight");
                    }
                }
                else
                {
                    Debug.Log("Not In Range, Max Range is " + I.MaxRange + " Min Range is " + I.MinRange + " Current distance is " + SpellChecks.DistanceCheck(Player, Target));
                }
            }
            else
            {
                Debug.Log("Not Enough Resources");
            }
        }
        else
        {
            Debug.Log("Not Attackable");
        }
    }

    void Update () {

        if (Input.GetKeyDown(KeyCode.L))
        {
            DoSpell();
        }
	
	}
}
