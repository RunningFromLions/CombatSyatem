﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[System.Serializable]
public class Attributes
{

    [System.Serializable]
    public enum StatName
    {
        Health,
        Speed,
        Power,
        Accuracy,
        Haste,
        Defense,
        Resources
    }

    [System.Serializable]
    public class Values
    {
        public int Base;
        public int Max;
        public int Current;
        public int Regen;
    }

    public StatName Stat;
    public Values Value;

    
}
