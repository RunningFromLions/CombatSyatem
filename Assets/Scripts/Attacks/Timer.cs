﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;

public class Timer {

    #region Variables
    public float StartTime;
    public float EndTime;
    public float Elapsed;

    public int Ticks;
    public int TicksRemaining;
    public float TickLength;
    public bool UnlimitedDuration;


    public SpellInstance Spell;

    public delegate void TimerDelegate(SpellInstance Spell);
    public event TimerDelegate Tick;
    public event TimerDelegate End;
    #endregion

    IEnumerator<float> Ticking(SpellInstance Spell)
    {
        while (TicksRemaining > 0)
        {
            if(Elapsed < TickLength)
            {
                Elapsed += Time.deltaTime;
                yield return 0f;
            }
            else
            {
                Elapsed = 0;
                TicksRemaining--;
                Tick(Spell);
                yield return 0f;
            }
        }
        End(Spell);
    }

    public void Cast(SpellInstance Spell)
    {
        Ticks = Spell.Ticks;
        TicksRemaining = Ticks;
        Elapsed = 0;
        StartTime = Time.time;
        EndTime = StartTime + Spell.Duration;
        TickLength = Spell.Duration / Ticks;

        Timing.RunCoroutine(Ticking(Spell));
    }

}
