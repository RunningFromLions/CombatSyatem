﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CreateAssetMenu(fileName = "SpellBook", menuName = "SpellBook", order = 1)]
[System.Serializable]
public class CreateSpellBook : ScriptableObject
{
    public SpellObject[] Spells;
    public List<CreateNewCharacter> Toons = new List<CreateNewCharacter>();
    public List<CreateNewDamageType> DamageTypes = new List<CreateNewDamageType>();
    public List<CreateNewResource> Resources = new List<CreateNewResource>();
    public List<CreateSplineProfile> Splines = new List<CreateSplineProfile>();
}

