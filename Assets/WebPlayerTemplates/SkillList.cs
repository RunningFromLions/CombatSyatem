﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;

public class SkillList : MonoBehaviour
{

    public List<SpellInstance> Spells = new List<SpellInstance>();
    public int SpellsOnCoolDown;

    CreateNewCharacter Toon;

    public bool GCD;
    public float GCD_Remaining;
    public float GlobalCD_Length;

    /// <summary>
    /// poulates the toon's ability list
    /// </summary>
    public void GetMoves()  //need to create instances of the attacks so we dont write data permanantly to the templates
    {
        Toon = gameObject.GetComponent<Stats>().Toon_Profile;
    
        for(int i = 0; i < Toon.Buffs.Count; i++)
        {
            Buffs.Add(null);
            Buffs[i] = Instantiate<CreateNewBuff>(Toon.Buffs[i]);
            Buffs[i].name = Toon.Buffs[i].name;
            Buffs[i].ReadyToCast = true;
        }

        for (int i = 0; i < Toon.DOTs.Count; i++)
        {
            DOTs.Add(null);
            DOTs[i] = Instantiate<CreateNewDOT>(Toon.DOTs[i]);
            DOTs[i].name = Toon.DOTs[i].name;
            DOTs[i].ReadyToCast = true;
        }

        for (int i = 0; i < Toon.DAttack.Count; i++)
        {
            DAttack.Add(null);
            DAttack[i] = Instantiate<CreatNewDirectAttack>(Toon.DAttack[i]);
            DAttack[i].name = Toon.DAttack[i].name;
            DAttack[i].ReadyToCast = true;
        }

        for (int i = 0; i < Toon.Shields.Count; i++)
        {
            Shields.Add(null);
            Shields[i] = Instantiate<CreateNewShield>(Toon.Shields[i]);
            Shields[i].name = Toon.Shields[i].name;
            Shields[i].ReadyToCast = true;
        }

        for (int i = 0; i < Toon.Projectiles.Count; i++)
        {
            Projectiles.Add(null);
            Projectiles[i] = Instantiate<CreateNewProjectile>(Toon.Projectiles[i]);
            Projectiles[i].name = Toon.Projectiles[i].name;
            Projectiles[i].ReadyToCast = true;
        }
    }

    void Start()
    {
        GetMoves();
        SpellsOnCoolDown = 0;
    }

    /// <summary>
    /// tracks which moves are on cooldown
    /// </summary>
    /// <param name="CD_Time"></param>
    /// <param name="Buff"></param>
    /// <param name="DOT"></param>
    /// <param name="Shield"></param>
    /// <param name="DAttack"></param>
    /// <param name="Projectile"></param>
    /// <returns></returns>
    public IEnumerator<float> CoolDownTimer(float CD_Time, SpellInstance Spell)
    {
        float Start = Time.time;
        float End = Start + CD_Time;

        while (End >= Time.time)
        {
            if (Spell != null)
            {
                Spell.Ready = false;
                float diff = End - Time.time;
                Spell.CD_Remaining = diff;
            }

            yield return 0f;
        }

        if (Spell != null)
        {
            Spell.Ready = true;
            Spell.CD_Remaining = 0;
        }

    }

    /// <summary>
    /// monitors and enforces the global cooldown
    /// </summary>
    /// <returns></returns>
    public IEnumerator<float> GCDTraacker()
    {
        GCD = true;
        float EndTime = Time.time + GlobalCD_Length;

        while (EndTime >= Time.time)
        {
            GCD_Remaining = EndTime - Time.time;
            yield return 0f;
        }

        GCD = false;
        GCD_Remaining = 0;
    }

    /// <summary>
    /// returns true if ability is on cooldown
    /// </summary>
    /// <param name="Source"></param>
    /// <param name="Buff"></param>
    /// <param name="DOT"></param>
    /// <param name="Shield"></param>
    /// <param name="DAttack"></param>
    /// <param name="Projectile"></param>
    /// <returns></returns>
    public static bool IsMoveOnCD(GameObject Source, SpellInstance Spell)
    {
        // the fucky for loop to find the index by comparing names is there because for some reason List.IndexOf always returns -1.

        bool ReadyToCast = false;
        SkillList Skills = Source.GetComponent<SkillList>();

        if (Spell != null)
        {
            int Index = -1;

            for (int i = 0; i < Skills.Spells.Count; i++)
            {
                string Name = Skills.Spells[i].name;
                if (Name == Spell.name)
                {
                    Index = i;
                }
            }
            ReadyToCast = Skills.Spells[Index].Ready;
        }
        return ReadyToCast;
    }

    /// <summary>
    /// starts the cooldown timer on an ability
    /// </summary>
    /// <param name="Source"></param>
    /// <param name="Buff"></param>
    /// <param name="DOT"></param>
    /// <param name="Shield"></param>
    /// <param name="DAttack"></param>
    /// <param name="Projectile"></param>
    public static void PutMoveOnCD(GameObject Source, SpellInstance Spell)
    {
        // the fucky for loop to find the index by comparing names is there because for some reason List.IndexOf always returns -1.

        SkillList Skills = Source.GetComponent<SkillList>();

        if (Spell != null)
        {
            int Index = -1;

            for (int i = 0; i < Skills.Spells.Count; i++)
            {
                string Name = Skills.Spells[i].name;
                if (Name == Spell.name)
                {
                    Index = i;
                }
            }

            float CD = Spell.CoolDown;
            SpellInstance mySpell = Skills.Spells[Index];
            Timing.RunCoroutine(Skills.CoolDownTimer(CD, mySpell));
        }
    }

    /// <summary>
    /// returns true if the global cooldown is in effect
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static bool OnGCD(GameObject Source)
    {
        bool OnGCD = Source.GetComponent<SkillList>().GCD;

        return OnGCD;
    }

}
