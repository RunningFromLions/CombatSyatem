﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[System.Serializable]
public class Collections {
    public int Gold;
    public int GoldOnKill;
    public int Kills;
    public int Deaths;
    public float DamageDealt;
    public float DamageTaken;
    public float HealingDone;
    public float HealingTaken;
    public GameObject LastAttackedBy;
}
