﻿using UnityEngine;
using System.Collections;

public class InputMonitor : MonoBehaviour {

    #region Delegates
    public delegate void KeyUpEvent(KeyCode Key = KeyCode.None, string Axis = null, float Time = 0, float Value = 0);
    public static event KeyUpEvent OnAnyKeyUp;

    public delegate void KeyDownEvent(KeyCode Key = KeyCode.None, string Axis = null, float Time = 0, float Value = 1);
    public static event KeyDownEvent OnAnyKeyDown;

    public delegate void KeyHeldEvent(KeyCode Key = KeyCode.None, string Axis = null, float Time = 0, float Value = 1);
    public static event KeyHeldEvent OnAnyKeyHeld;

    public delegate void ActiveAxisEvent(KeyCode Key = KeyCode.None, string Axis = null, float Time = 0, float Value = 1);
    public static event ActiveAxisEvent OnAnyAxisActive;

    public delegate void EndAxisEvent(KeyCode Key = KeyCode.None, string Axis = null, float Time = 0, float Value = 0);
    public static event EndAxisEvent OnAnyAxisEnd;
    #endregion

    #region Variables
    public InputManager Manager;
    public KeyCode myKey;
    public string myAxis;
    public float myValue;
    public bool Active;
    public float ActiveFor;
    public float DeadZone
    {
        get
        {
            if(myAxis == "Mouse X" || myAxis == "Mouse Y")
            {
                return 0;
            }
            else
            {
                return Manager.JoystickDeadZone;
            }
        }
    }

    bool isKey;
    #endregion

    /// <summary>
    /// checks for key up, key down, and tracks how long a key is held
    /// </summary>
    void MonitorKey()
    {
        #region Monitor Key
        if (myKey != KeyCode.None)
        {

            if (Input.GetKeyDown(myKey))
            {
                OnAnyKeyDown(myKey);
            }

            if (Input.GetKeyUp(myKey))
            {
                OnAnyKeyUp(myKey);
            }

            if (!Active)
            {
                if (Input.GetKey(myKey))
                {
                    Active = true;
                    myValue = 1;
                }
            }

            if (Active)
            {
                Timer();
                OnAnyKeyHeld(myKey, null, ActiveFor);
                if (!Input.GetKey(myKey))
                {
                    Active = false;
                    myValue = 0;
                    ActiveFor = 0;
                }
            }
        }
        #endregion
    }

    /// <summary>
    /// checks for axis input exceeding the Deadzone Limit and tracks how long it is active
    /// </summary>
    void MonitorAxis()
    {
        #region Monitor Axis

        if(myAxis != "Cancel" && myAxis != "")
        {
            if (Mathf.Abs(Input.GetAxis(myAxis)) > DeadZone)
            {
                myValue = Input.GetAxis(myAxis);

                if (!Active)
                {
                    Active = true;
                }
                else
                {
                    Timer();
                    OnAnyAxisActive(KeyCode.None, myAxis, ActiveFor, myValue);
                }
            }
            else
            {
                if (Active)
                {
                    Active = false;
                    ActiveFor = 0;
                    myValue = 0;
                    OnAnyAxisEnd(KeyCode.None, myAxis, ActiveFor, myValue);
                }
            }
        }
        #endregion
    }

    /// <summary>
    /// tracks active time
    /// </summary>
    void Timer()
    {
        ActiveFor += Time.deltaTime;
    }

    void Update () {


        if (myKey != KeyCode.None)
        {
            isKey = true;
        }

        if (isKey)
        {
            MonitorKey();
        }
        else
        {
            MonitorAxis();
        }

    }
}
