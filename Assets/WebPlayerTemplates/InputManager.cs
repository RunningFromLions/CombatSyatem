﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;


public class InputManager : MonoBehaviour {

    #region Variables
    public CreateKeybinds KeyBinds;

    List<InputMonitor> InputMonitors = new List<InputMonitor>();
    public Dictionary<string, InputNode> Nodes = new Dictionary<string, InputNode>();

    [Range(0,1)]
    public float JoystickDeadZone;

    KeyCode[] AllKeys;
    string[] KeyNames;
    public List<string> ValidKeybinds = new List<string>();

    #endregion

    /// <summary>
    /// generate a list holding all possible keys, used for mathing
    /// </summary>
    void ListOfKeys()
    {
        AllKeys = new KeyCode[Enum.GetValues(typeof(KeyCode)).Length];
        KeyNames = new string[AllKeys.Length];

        int i = 0;
        foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
        {
            AllKeys[i] = kcode;
            KeyNames[i] = kcode.ToString();
            i++;
        }
    }

    /// <summary>
    /// matches string name to KeyCode 
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    KeyCode MatchByName(string Name)
    {
        KeyCode match = KeyCode.None;

        for(int i = 0; i < KeyNames.Length; i++)
        {
            if(Name == KeyNames[i])
            {
                match = AllKeys[i];
            }
        }

        return match;
    }

    /// <summary>
    /// checks for needed amount of InputMonitors and spawns them by calling CreateMonitor
    /// </summary>
    void MonitorCheck()
    {
        FieldInfo[] fields;
        Type myType = typeof(CreateKeybinds);

        fields = myType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

        for (int i = 0; i < fields.Length; i++)
        {
            if (fields[i].FieldType == typeof(System.String))
            {
                string Name = fields[i].Name.ToString();
                string Axis = fields[i].GetValue(KeyBinds).ToString();
                //Debug.Log(Name + " " + Axis);
                ValidKeybinds.Add(Name);
                CreateMonitor(Name, Axis);

            }

            if (fields[i].FieldType == typeof(UnityEngine.KeyCode))
            {
                string Name = fields[i].Name.ToString();
                string code = fields[i].GetValue(KeyBinds).ToString();
                KeyCode Key = MatchByName(code);
                ValidKeybinds.Add(Name);
                CreateMonitor(Name, "Cancel", Key);
            }
        }
    }

    /// <summary>
    /// spawns a new InputMonitor
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="Axis"></param>
    /// <param name="KC"></param>
    void CreateMonitor(string Name, string Axis = "Cancel", KeyCode KC = KeyCode.None)
    {
        InputMonitor newMonitor = gameObject.AddComponent<InputMonitor>();
        newMonitor.Manager = gameObject.GetComponent<InputManager>();
        newMonitor.myAxis = Axis;
        newMonitor.myKey = KC;
        newMonitor.Active = false;
        InputMonitors.Add(newMonitor);

        InputNode n = new InputNode();
        n.Name = Name;
        n.Monitor = newMonitor;
        Nodes.Add(Name, n);
    }

    void OnEnable()
    {
        InputMonitor.OnAnyKeyDown += Test;
        InputMonitor.OnAnyKeyUp += Test;
        InputMonitor.OnAnyKeyHeld += Test;
        InputMonitor.OnAnyAxisActive += Test;
        InputMonitor.OnAnyAxisEnd += Test;
    }

    #region Delegate driven methods

    void Test(KeyCode Key = KeyCode.None, string Axis = null, float Value = 0, float Time = 0)
    {
       //Debug.Log(Key + " " + Axis + " " + Value + " " + Time);
    }

    #endregion

    void Start () {
        ListOfKeys();
        MonitorCheck();
    }

    void OnDisable()
    {
        InputMonitor.OnAnyKeyDown -= Test;
        InputMonitor.OnAnyKeyUp -= Test;
        InputMonitor.OnAnyKeyHeld -= Test;
        InputMonitor.OnAnyAxisActive -= Test;
        InputMonitor.OnAnyAxisEnd -= Test;
    }



}
