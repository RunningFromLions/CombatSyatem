﻿using UnityEngine;
using System.Collections;

public class InputNode {

        public InputMonitor Monitor;
        public string Name;
        public KeyCode myKey
        {
            get
            {
                return Monitor.myKey;
            }
        }
        public string Axis
        {
            get
            {
                return Monitor.myAxis;
            }
        }
        public float Value
        {
            get
            {
                return Monitor.myValue;
            }
        }
        public bool Active
        {
            get
            {
                return Monitor.Active;
            }
        }
        public float ActiveFor
        {
            get
            {
                return Monitor.ActiveFor;
            }
        }
}
