﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;

public class MovementForces : MonoBehaviour
{

    MovementManager MMan;
    StateMonitor SMon;
    InputManager IMan;
    Rigidbody Rb;
    Vector3 Direction
    {
        get
        {
            return MMan.Direction;
        }
    }
    float CurrentSpeed
    {
        get
        {
            return MMan.CurrentSpeed;
        }
    }
    float Magnitude
    {
        get
        {
            return MMan.Magnitude;
        }
    }
    public bool ZeroAccelMode;
    public bool ForcesDuringJump;
    public bool WorldUpJump;
    public bool DoubleJump;

    [Range(1,5)]
    public int NumJumps;

    [Range(0,2)]
    public float TimeBetweenJumps;
    bool CanJump;

    int Jumps;

    [Range(1,20)]
    public float JumpForce;

    [Range(0.1f, 2)]
    public float AccelerationIntensity;

    [Range(1, 20)]
    public float BreakingIntensity;

    void OnEnable()
    {
        InputMonitor.OnAnyKeyDown += Jump;
        //InputMonitor.OnAnyKeyUp += Jump;
        //InputMonitor.OnAnyKeyHeld += Jump;
        //InputMonitor.OnAnyAxisActive += Jump;
        //InputMonitor.OnAnyAxisEnd += Jump;
    }

    void Start()
    {
        MMan = gameObject.GetComponent<MovementManager>();
        SMon = gameObject.GetComponent<StateMonitor>();
        Rb = MMan.Player.GetComponentInChildren<Rigidbody>();
        IMan = gameObject.GetComponent<InputManager>();

    }

    void StopLateralMovemntNow()
    {
        if (SMon.Idle && SMon.Grounded)
        {
            Rb.velocity = new Vector3(0, Rb.velocity.y, 0);
        }
    }

    void LateralBreaking()
    {
        if (SMon.Idle && SMon.Grounded)
        {
            Vector3 BreakDir = new Vector3(Rb.velocity.x, 0, Rb.velocity.z);
            Rb.AddForce(-BreakDir * BreakingIntensity);
        }
    }

    void ApplyInstantLateralForces()
    {
        #region Walking/Running
        if (SMon.Walk | SMon.Sprint)
        {
            if (SMon.Grounded)
            {
                Rb.AddForce(Direction, ForceMode.VelocityChange);
                Vector3 Clamp = Vector3.ClampMagnitude(Rb.velocity, CurrentSpeed);
                Rb.velocity = new Vector3(Clamp.x, Rb.velocity.y, Clamp.z);
            }
        }
        #endregion

        #region Falling
        if (SMon.Falling | SMon.Jumping)
        {
            if (!SMon.Grounded)
            {
                if (ForcesDuringJump)
                {
                    Rb.AddForce(Direction, ForceMode.VelocityChange);
                    Vector3 Clamp = Vector3.ClampMagnitude(Rb.velocity, CurrentSpeed);
                    Rb.velocity = new Vector3(Clamp.x, Rb.velocity.y, Clamp.z);
                }
            }
        }
        #endregion

        #region Idle
        if (SMon.Idle)
        {
            StopLateralMovemntNow();
        }
        #endregion
    }

    void ApplySmoothLateralForces()
    {
        #region Walking/Running
        if (SMon.Walk | SMon.Sprint)
        {
            if (SMon.Grounded)
            {
                Rb.AddForce(Direction * (CurrentSpeed * AccelerationIntensity));
                Vector3 Clamp = Vector3.ClampMagnitude(Rb.velocity, CurrentSpeed);
                Rb.velocity = new Vector3(Clamp.x, Rb.velocity.y, Clamp.z);
            }
        }
        #endregion

        #region Falling
        if (SMon.Falling | SMon.Jumping)
        {
            if (!SMon.Grounded)
            {
                if (ForcesDuringJump)
                {
                    Rb.AddForce(Direction * (CurrentSpeed * AccelerationIntensity));
                    Vector3 Clamp = Vector3.ClampMagnitude(Rb.velocity, CurrentSpeed);
                    Rb.velocity = new Vector3(Clamp.x, Rb.velocity.y, Clamp.z);
                }
            }
        }
        #endregion

        #region Idle
        if (SMon.Idle)
        {
            LateralBreaking();
        }
        #endregion
    }

    void Jump(KeyCode Key = KeyCode.None, string Axis = null, float Value = 0, float Time = 0)
    {
        if(Key == IMan.KeyBinds.Jump | Key == IMan.KeyBinds.Controller_Jump)
        {
            if (CanJump)
            {
                Timing.RunCoroutine(JumpTimer(TimeBetweenJumps));

                if (SMon.Grounded)
                {
                    Jumps = NumJumps;
                }
                else
                {
                    Jumps--;
                }

                if (Jumps > 0)
                {
                    if (WorldUpJump)
                    {
                        Rb.AddForce(Vector3.up * JumpForce, ForceMode.Impulse);
                    }
                    else
                    {
                        Rb.AddForce(MMan.Player.transform.up * JumpForce, ForceMode.Impulse);
                    }
                }
            }
        }
    }

    public IEnumerator<float> JumpTimer(float seconds)
    {
        float Start = Time.time;
        float End = Start + seconds;
        while(Time.time < End)
        {
            CanJump = false;
            yield return 0f;
        }
        CanJump = true;
    }

    void FixedUpdate()
    {
        if (ZeroAccelMode)
        {
            ApplyInstantLateralForces();
        }
        else
        {
            ApplySmoothLateralForces();
        }

        if (SMon.Grounded)
        {
            CanJump = true;
        }
    }

    void OnDisable()
    {
        InputMonitor.OnAnyKeyDown -= Jump;
    }
}
