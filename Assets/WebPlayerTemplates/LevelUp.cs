﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelUp : MonoBehaviour {

    public int EXP;
    public int EXPonKill;
    public AnimationCurve XPCurve;
    public List<int> EXPtoLevel = new List<int>();
    public int EXPMultiplier;

}
