﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(InputManager))]
[RequireComponent(typeof(StateMonitor))]
public class MovementManager : MonoBehaviour {

    public InputManager Man;
    public GameObject Player;
    Rigidbody Rb;
    public StateMonitor Smon;
    Vector3 Forward
    {
        get
        {
            return Player.transform.forward;
        }
    }
    Vector3 Backward
    {
        get
        {
            return -Forward;
        }
    }
    Vector3 Right
    {
        get
        {
            return Player.transform.right;
        }
    }
    Vector3 Left
    {
        get
        {
            return -Right;
        }
    }
    Vector3 Idle
    {
        get
        {
            return Vector3.zero;
        }
    }

    public Vector3[] Vectors = new Vector3[4];
    public Vector3 Direction;
    public Vector3 Velocity;
    public float Magnitude;

    public float CurrentSpeed;

    [Range(1, 100)]
    public float MaxSpeed;

    [Range(0, 1)]
    public float WalkSpeed;

    // Use this for initialization
    void Start () {
        Man = gameObject.GetComponent<InputManager>();
        Smon = gameObject.GetComponent<StateMonitor>();
        Rb = Player.GetComponentInChildren<Rigidbody>();
	}

    // gets input information from Mon.Nodes[] and converts to directions
    void PopulateVectors()
    {
        #region Forward Vectors
        if (Man.Nodes["Forward"].Active)
        {
            Vectors[0] = Forward;
        }
        else
        {
            if (Man.Nodes["VerticalJoystick"].Active)
            {
                float Value = Man.Nodes["VerticalJoystick"].Value;
                if(Value < 0)
                {
                    Vectors[0] = Forward * (Mathf.Abs(Value));
                }
            }
            else
            {
                Vectors[0] = Idle;
            }
        }
        #endregion

        #region Backward Vectors
        if (Man.Nodes["Back"].Active)
        {
            Vectors[1] = Backward;
        }
        else
        {
            if (Man.Nodes["VerticalJoystick"].Active)
            {
                float Value = Man.Nodes["VerticalJoystick"].Value;
                if(Value > 0)
                {
                    Vectors[1] = Backward * (Mathf.Abs(Value));
                }
            }
            else
            {
                Vectors[1] = Idle;
            }
        }
        #endregion

        #region Left Vectors
        if (Man.Nodes["Left"].Active)
        {
            Vectors[2] = Left;
        }
        else
        {
            if (Man.Nodes["HorizontalJoystick"].Active)
            {
                float Value = Man.Nodes["HorizontalJoystick"].Value;
                if (Value < 0)
                {
                    Vectors[2] = Left * (Mathf.Abs(Value));
                }
            }
            else
            {
                Vectors[2] = Idle;
            }
        }
        #endregion

        #region Right Vectors
        if (Man.Nodes["Right"].Active)
        {
            Vectors[3] = Right;
        }
        else
        {
            if (Man.Nodes["HorizontalJoystick"].Active)
            {
                float Value = Man.Nodes["HorizontalJoystick"].Value;
                if (Value > 0)
                {
                    Vectors[3] = Right * (Mathf.Abs(Value));
                }
            }
            else
            {
                Vectors[3] = Idle;
            }
        }
        #endregion
    }

    // gets currentspeed based on Analog stick movement
    // goes straight to walk speed if KB is used
    void GetSpeed()
    {
        if(Smon.Locomotion_State == StateMonitor.Locomotion.Idle)
        {
            CurrentSpeed = 0;
        }

        if (Smon.Locomotion_State == StateMonitor.Locomotion.Walk)
        {
            if(Man.Nodes["VerticalJoystick"].Active | Man.Nodes["HorizontalJoystick"].Active)
            {
                float AnalogV = Mathf.Abs(Man.Nodes["VerticalJoystick"].Value);
                float AnalogH = Mathf.Abs(Man.Nodes["HorizontalJoystick"].Value);
                float AnalogValue = AnalogH + AnalogV;
                AnalogValue = Mathf.Clamp(AnalogValue, 0, 1);

                CurrentSpeed = MaxSpeed * (WalkSpeed * AnalogValue);
            }
            else
            {
                CurrentSpeed = MaxSpeed * WalkSpeed;
            }
        }

        if (Smon.Locomotion_State == StateMonitor.Locomotion.Sprint)
        {
            CurrentSpeed = MaxSpeed;
        }


    }

    // sum of angles in Vectors[] normalized
    Vector3 CalculateDirection()
    {
        PopulateVectors();

        Vector3 dir = Idle;

        for(int i = 0; i < Vectors.Length; i++)
        {
            if(Vectors[i] != Idle)
            {
                dir = (dir + Vectors[i]);
            }
        }

        return dir.normalized * CurrentSpeed;
    }

    // Update is called once per frame
    void Update () {

        GetSpeed();
        Direction = CalculateDirection();
        Velocity = Rb.velocity;
        Magnitude = Rb.velocity.magnitude;
	}

    void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawLine(Player.transform.position, Player.transform.position + (Direction* CurrentSpeed));
    }
}
