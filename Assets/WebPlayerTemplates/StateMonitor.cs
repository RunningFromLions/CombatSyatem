﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;

public class StateMonitor : MonoBehaviour {

    public enum Locomotion
    {
        Idle,
        Walk,
        Sprint
    };
    public bool Walk
    {
        get
        {
            bool Walking = false;

            if (IMan.Nodes["Forward"].Active |
                IMan.Nodes["Back"].Active |
                IMan.Nodes["Left"].Active |
                IMan.Nodes["Right"].Active |
                IMan.Nodes["VerticalJoystick"].Active |
                IMan.Nodes["HorizontalJoystick"].Active)
            {
                Walking = true;
            }

            return Walking;
        }
    }
    public bool Sprint
    {
        get
        {
            bool Sprinting = false;

            if(IMan.Nodes["Sprint"].Active | IMan.Nodes["Controller_Sprint"].Active)
            {
                if (Walk)
                {
                    Sprinting = true;
                }
            }
            return Sprinting;
        }
    }
    public bool Idle
    {
        get
        {
            bool idle = true;

            if (Walk | Sprint)
            {
                idle = false;
            }
            return idle;
        }
    }
    public bool Moving;

    Locomotion locomotion_State
    {
        get
        {
            Locomotion state = Locomotion.Idle;

            if (Idle)
            {
                state = Locomotion.Idle;
            }

            if (Walk)
            {
                if (!Sprint)
                {
                    state = Locomotion.Walk;
                }
                else
                {
                    state = Locomotion.Sprint;
                }
            }

            return state;
        }
    }
    public Locomotion Locomotion_State;

    public enum Posture
    {
        Lay,
        Kneel,
        Sit,
        Crouch,
        Stand
    };

    public enum Ground
    {
        Falling,
        Grounded,
        Jumping,
        Flying
    };
    public bool Grounded
    {
        get
        {
            return GroundedCheck();
        }
    }
    public bool Falling
    {
        get
        {
            bool falling = false;

            if (!GroundedCheck())
            {
                if(MMan.Velocity.y < -.5f)
                {
                    falling = true;
                }
            }

            return falling;
        }
    }
    public bool Jumping
    {
        get
        {
            bool jumping = false;

            if (!GroundedCheck())
            {
                if(MMan.Velocity.y > .5f)
                {
                    jumping = true;
                }
            }
            return jumping;
        }
    }

    Ground grounded_State
    {
        get
        {
            Ground state = Ground.Grounded;

            if (Grounded)
            {
                state = Ground.Grounded;
            }

            if (Falling)
            {
                state = Ground.Falling;
            }

            if (Jumping)
            {
                state = Ground.Jumping;
            }

            return state;
        }
    }
    public Ground Grounded_State;

    GameObject Player;
    InputManager IMan;
    MovementManager MMan;

    public LayerMask GroundLayer;

    [Range(0,10)]
    public float RaycastVerticalOffset;

    [Range(0, 5)]
    public float GroundedTolerance;

    public bool GroundedCheck()
    {
        bool onGround = false;
        RaycastHit hit;
        Vector3 VerticalOffset = Vector3.up * RaycastVerticalOffset;
        if(Physics.Raycast(Player.transform.position + VerticalOffset, -Vector3.up, out hit, GroundLayer))
        {
            Vector3 point = hit.point;
            float Distance = Vector3.Distance(Player.transform.position, point);
            if(Distance < GroundedTolerance)
            {
                onGround = true;
            }
        }
        return onGround;
    }

    void MoveCheck()
    {
        Timing.RunCoroutine(MovementCheck(.1f));
    }
    Vector3 CheckPoint()
    {
        RaycastHit hit;
        Vector3 point1 = Vector3.zero;

        Vector3 VerticalOffset = Vector3.up * RaycastVerticalOffset;
        if (Physics.Raycast(Player.transform.position + VerticalOffset, -Vector3.up, out hit, Mathf.Infinity))
        {
            point1 = hit.point;
        }
        return point1;
    }
    IEnumerator<float> MovementCheck(float seconds)
    {
        float st = Time.time;
        float et = st + seconds;
        Vector3 p1 = CheckPoint();
        Vector3 VerticalOffset = Vector3.up * RaycastVerticalOffset;
        float D1 = Vector3.Distance(Player.transform.position + VerticalOffset, p1);
        while (Time.time < et)
        {
            yield return 0f;
        }
        Vector3 p2 = CheckPoint();
        float D2 = Vector3.Distance(Player.transform.position + VerticalOffset, p2);
        if (p1 == p2 && D1 == D2)
        {
            Moving = false;
        }
        else
        {
            Moving = true;
        }
    }

    void Start()
    {
        IMan = gameObject.GetComponent<InputManager>();
        Player = gameObject.GetComponent<MovementManager>().Player;
        MMan = gameObject.GetComponent<MovementManager>();
        InvokeRepeating("MoveCheck",0,.1f);
    }

    void Update()
    {
        Locomotion_State = locomotion_State;
    }

    void FixedUpdate()
    {
        Grounded_State = grounded_State;
    }

    void OnDisable()
    {
        CancelInvoke("MoveCheck");
    }
}
