﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(CreateNewFaction))]
public class FactionEditor : Editor {

    #region variables
    public List<CreateNewFaction.Names> AllNames = new List<CreateNewFaction.Names>();
    int[] friend = new int[20];
    int[] neutral = new int[20];
    int[] foe = new int[20];
    #endregion

    #region GUIedit params
    float ScreenWidth;
    float LabelSize;
    float Popupsize;
    float spacing;
    float LabelSpacing;
    float labelshift;
    float PUShift;
    float LST;
    float PUT;
    float ST;
    float ShiftT;
    float PUST;
    float LabelSpaceT;

    #endregion

    public override void OnInspectorGUI()
    {
        CreateNewFaction F = (CreateNewFaction)target;

        GetAllFactions();
        DrawElements();
        GUIEdit();

        #region GUI change options
        if (GUI.changed)
        {
            EditorUtility.SetDirty(F);
        }
        //base.OnInspectorGUI();
        #endregion
    }

    void GetAllFactions()
    {
        CreateNewFaction F = (CreateNewFaction)target;

        #region foreach loop to count all faction names
        int Count = 0;
        foreach(CreateNewFaction.Names Faction in System.Enum.GetValues(typeof(CreateNewFaction.Names)))
        {
            Count++;
        }
        #endregion

        #region if all names does not have the right number of faction names, clear it and update it
        if (AllNames.Count < Count)
        {
            AllNames.Clear();
            foreach (CreateNewFaction.Names Faction in System.Enum.GetValues(typeof(CreateNewFaction.Names)))
            {
                AllNames.Add(Faction);
            }
        }
        #endregion
    }

    void DrawElements()
    {
        CreateNewFaction F = (CreateNewFaction)target;

        #region Draw Name
        F.Name = (CreateNewFaction.Names)EditorGUILayout.EnumPopup("Faction Name", F.Name);
        #endregion

        #region Draw Labels
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("", GUILayout.MaxWidth(labelshift));

        EditorGUILayout.LabelField("Allies", GUILayout.MaxWidth(LabelSize));
        EditorGUILayout.LabelField("", GUILayout.MaxWidth(LabelSpacing));

        EditorGUILayout.LabelField("Neutral", GUILayout.MaxWidth(LabelSize));
        EditorGUILayout.LabelField("", GUILayout.MaxWidth(LabelSpacing));

        EditorGUILayout.LabelField("Enemies", GUILayout.MaxWidth(LabelSize));
        EditorGUILayout.EndHorizontal();
        #endregion

        #region convert enum list to string array for options pop up
        string[] choices = new string[AllNames.Count];
        for (int i = 0; i < AllNames.Count; i++)
        {
            choices[i] = AllNames[i].ToString();
        }
        #endregion

        #region Populate lists if needed
        for(int i = 0; i < AllNames.Count; i++)
        {
            if(F.FriendList.Count < AllNames.Count)
            {
                F.FriendList.Add(AllNames[0]);
            }

            if (F.NeutralList.Count < AllNames.Count)
            {
                F.NeutralList.Add(AllNames[0]);
            }


            if (F.FoeList.Count < AllNames.Count)
            {
                F.FoeList.Add(AllNames[0]);
            }
        }
        #endregion

        #region Match editor arrays w/ object lists
        for(int i = 0; i < AllNames.Count; i++)
        {
            friend[i] = AllNames.IndexOf(F.FriendList[i]);
            neutral[i] = AllNames.IndexOf(F.NeutralList[i]);
            foe[i] = AllNames.IndexOf(F.FoeList[i]);
        }
        #endregion

        #region draw pop ups
        for (int i = 0; i < AllNames.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("", GUILayout.MaxWidth(PUShift));

            friend[i] = EditorGUILayout.Popup(friend[i], choices, GUILayout.MaxWidth(Popupsize));
            F.FriendList[i] = AllNames[friend[i]];

            EditorGUILayout.LabelField("", GUILayout.MaxWidth(spacing));

            neutral[i] = EditorGUILayout.Popup(neutral[i], choices, GUILayout.MaxWidth(Popupsize));
            F.NeutralList[i] = AllNames[neutral[i]];

            EditorGUILayout.LabelField("", GUILayout.MaxWidth(spacing));

            foe[i] = EditorGUILayout.Popup(foe[i], choices, GUILayout.MaxWidth(Popupsize));
            F.FoeList[i] = AllNames[foe[i]];

            EditorGUILayout.EndHorizontal();
        }
        #endregion

        DrawDefaultInspector();
    }

    void GUIEdit()
    {
        ScreenWidth = Screen.width;
      //LST = EditorGUILayout.Slider("Label Size", LST, 0, 1);
      //PUT = EditorGUILayout.Slider("Pup Up Size",PUT, 0, 1);
      //ST = EditorGUILayout.Slider("Pop Up Spaces", ST, 0, 1);
      //ShiftT = EditorGUILayout.Slider("Label Shift Left", ShiftT, 0, 1);
      //PUST = EditorGUILayout.Slider("Pop Up Shift Left", PUST, 0, 1);
      //LabelSpaceT = EditorGUILayout.Slider("Label Spaces", LabelSpaceT, 0, 1);

        LST = 0.124f;
        PUT = 0.224f;
        ST = 0.104f;
        ShiftT = 0.071f;
        PUST = 0.01f;
        LabelSpaceT = 0.188f;

        LabelSize = ScreenWidth * LST;
        Popupsize = ScreenWidth * PUT;
        spacing = ScreenWidth * ST;
        labelshift = ScreenWidth * ShiftT;
        PUShift = ScreenWidth * PUST;
        LabelSpacing = ScreenWidth * LabelSpaceT;
    }
}
