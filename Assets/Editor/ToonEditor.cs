﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(CreateNewCharacter))]
public class ToonEditor : Editor
{

    #region Foldout bools
    public bool Basic;
    public bool Evo;
    public bool Type;
    public bool Resources;
    public bool Appearance;
    public bool Stats;
    public bool Regen;
    public bool Audio;
    public bool Abilities;
    #endregion

    public override void OnInspectorGUI()
    {
        CreateNewCharacter Toon = (CreateNewCharacter)target;

        #region method invocation order
        #region Basic Info
        Basic = EditorGUILayout.Foldout(Basic, "Basic Information");
        if (Basic)
        {
            BasicInfo();
        }
        #endregion

        #region Evolution Info
        Evo = EditorGUILayout.Foldout(Evo, "Evolution Information");
        if (Evo)
        {
            EvolutionInfo();
        }
        #endregion

        #region Type Information
        Type = EditorGUILayout.Foldout(Type, "Elemental Type Selection");
        if (Type)
        {
            ElementalInfo();
        }
        #endregion

        #region Recource Information
        Resources = EditorGUILayout.Foldout(Resources, "Resource Information");
        if (Resources)
        {
            ResourceInfo();
        }
        #endregion

        #region Appearance information
        Appearance = EditorGUILayout.Foldout(Appearance, "Appearance Information");
        if (Appearance)
        {
            AppearanceInfo();
        }
        #endregion

        #region Stats
        Stats = EditorGUILayout.Foldout(Stats, "View/Edit Stats");
        if (Stats)
        {
            StatsInfo();
        }
        #endregion

        #region Regen Info
        Regen = EditorGUILayout.Foldout(Regen, "Stat Regen Information");
        if (Regen)
        {
            RegenInfo();
        }
        #endregion

        #region Audio Information
        Audio = EditorGUILayout.Foldout(Audio, "Audio Clip Selection");
        if (Audio)
        {
            AudioInfo();
        }
        #endregion

        #region Abilities
        Abilities = EditorGUILayout.Foldout(Abilities, "Ability Selection");
        if (Abilities)
        {
            AbilityInfo();
        }
        #endregion

        #endregion

        #region guichange
        //base.OnInspectorGUI();

        if (GUI.changed)
        {
            EditorUtility.SetDirty(Toon);
        }
        #endregion
    }

    public void BasicInfo()
    {
        CreateNewCharacter Toon = (CreateNewCharacter)target;

        #region Basic Info
        EditorGUILayout.HelpBox("Basic Info", MessageType.None);
        Toon.ToonID = EditorGUILayout.TextField("Toon ID", Toon.ToonID);
        Toon.Toon_Name = EditorGUILayout.TextField("Toon Name", Toon.Toon_Name);
        Toon.Toon_Description = EditorGUILayout.TextField("Toon Description", Toon.Toon_Description);
        #endregion
    }

    public void EvolutionInfo()
    {
        CreateNewCharacter Toon = (CreateNewCharacter)target;

        #region Evolution Info
        EditorGUILayout.Space();
        EditorGUILayout.HelpBox("Evolution Info", MessageType.None);

        #region if levelinfo is empty, ass a new CreateNewCharacter.LevelUp class so the editor doesnt bug out
        if (Toon.LevelInfo == null)
        {
            Toon.LevelInfo = new CreateNewCharacter.LevelUp();
        }
        #endregion

        #region Populate previous and next evolution so the editor doesnt yell at you
        if (Toon.LevelInfo.PreviousEvolution == null)
        {
            Toon.LevelInfo.PreviousEvolution = new CreateNewCharacter();
        }

        if (Toon.LevelInfo.PreviousEvolution == null)
        {
            Toon.LevelInfo.NextEvolution = new CreateNewCharacter();
        }
        #endregion

        // this is set up with an animation curve so we can try out having a dynamic leveling curve
        Toon.LevelInfo.XPCurve = EditorGUILayout.CurveField("XP Leveling Curve", Toon.LevelInfo.XPCurve);

        Toon.LevelInfo.PreviousEvolution = (CreateNewCharacter)EditorGUILayout.ObjectField("Previous Evolution", Toon.LevelInfo.PreviousEvolution, typeof(CreateNewCharacter), true);
        Toon.LevelInfo.NextEvolution = (CreateNewCharacter)EditorGUILayout.ObjectField("Next Evolution", Toon.LevelInfo.NextEvolution, typeof(CreateNewCharacter), true);
        Toon.LevelInfo.NextEvolutionLevel = EditorGUILayout.IntField("Next Evolution Level", Toon.LevelInfo.NextEvolutionLevel);
        #endregion
    }

    public void ElementalInfo()
    {
        CreateNewCharacter Toon = (CreateNewCharacter)target;

        #region Elemental Type
        EditorGUILayout.Space();
        EditorGUILayout.HelpBox("Elemental Type", MessageType.None);
        Toon.Type = (CreateNewDamageType)EditorGUILayout.ObjectField("Type", Toon.Type, typeof(CreateNewDamageType), false);
        #endregion
    }

    public void ResourceInfo()
    {
        CreateNewCharacter Toon = (CreateNewCharacter)target;

        #region Resource Info
        EditorGUILayout.Space();
        EditorGUILayout.HelpBox("Resource Type/Color", MessageType.None);

        EditorGUILayout.BeginHorizontal();
        Toon.ResourceType = (CreateNewResource)EditorGUILayout.ObjectField(Toon.ResourceType, typeof(CreateNewResource), false);
        if (Toon.ResourceType)
        {
            Color ResourceColor = Toon.ResourceType.ResourceBarColor;
            EditorGUILayout.ColorField(ResourceColor);
        }
        EditorGUILayout.EndHorizontal();
        #endregion
    }

    public void AppearanceInfo()
    {
        CreateNewCharacter Toon = (CreateNewCharacter)target;

        #region Appearance
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.HelpBox("Appearance", MessageType.None);

        Toon.Icon = (Sprite)EditorGUILayout.ObjectField("Toon GUI Icon", Toon.Icon, typeof(Sprite), false);
        Toon.Model = (GameObject)EditorGUILayout.ObjectField("Toon Model/Prefab", Toon.Model, typeof(GameObject), false);

        // added these vector3s so we can try to have some control as to where the spell effect originate and terminate
        // so we can avoid some weird situations where a charater appears to be shitting out particle effects
        // or likewise makign sue that every attack does target the back of their head or ass or some shit
        Toon.SpellOrigin = EditorGUILayout.Vector3Field("Attack Origin", Toon.SpellOrigin);
        Toon.SpellDestination = EditorGUILayout.Vector3Field("Attack Destination", Toon.SpellDestination);

        #endregion
    }

    public void StatsInfo()
    {
        // if you need to add another stat, add a new enum to the Attributes.StatName field
        // the editor will pouplate the field automatically
        CreateNewCharacter Toon = (CreateNewCharacter)target;

        #region Stats
        EditorGUILayout.Space();
        EditorGUILayout.HelpBox("Stats", MessageType.None);

        #region get the number of stats currently chilling in Attributes.StatName
        int Count = 0;
        foreach (Attributes.StatName stat in System.Enum.GetValues(typeof(Attributes.StatName)))
        {
            Count++;
        }
        #endregion

        #region if that number is different that what is currently in Toon.Stats, whipe Toon.Stats and re-populate
        if (Toon.Stats.Count != Count)
        {
            Toon.Stats.Clear();
            foreach (Attributes.StatName stat in System.Enum.GetValues(typeof(Attributes.StatName)))
            {
                Attributes A = new Attributes();
                A.Stat = stat;
                A.Value = new Attributes.Values();
                Toon.Stats.Add(A);
            }
        }
        #endregion

        #region for i loop to draw the StatName and the Values
        for (int i = 0; i < Toon.Stats.Count; i++)
        {
            string stat = Toon.Stats[i].Stat.ToString();

            Attributes.Values V = Toon.Stats[i].Value;
            EditorGUILayout.HelpBox(stat, MessageType.None);

            V.Base = EditorGUILayout.IntField("Base " + stat, V.Base);
            V.Max = EditorGUILayout.IntField("Max " + stat, V.Max);
            V.Current = EditorGUILayout.IntField("Current " + stat, V.Current);
            V.Regen = EditorGUILayout.IntField("Regeneration Rate " + stat, V.Regen);
        }
        #endregion

        #endregion
    }

    public void RegenInfo()
    {
        CreateNewCharacter Toon = (CreateNewCharacter)target;

        #region Regen - Nothing Here yet
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Base Resource Regen Per Tick");
        // Toon.Base_Resource_Regen = EditorGUILayout.FloatField(Toon.Base_Resource_Regen, GUILayout.MaxWidth(64));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Base Health Regen Per Tick");
        // Toon.Base_Health_Regen = EditorGUILayout.FloatField(Toon.Base_Health_Regen, GUILayout.MaxWidth(64));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Experience granted upon death");
        // Toon.Experience = EditorGUILayout.FloatField(Toon.Experience, GUILayout.MaxWidth(64));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Resources granted upon death");
        //  Toon.Gold = EditorGUILayout.FloatField(Toon.Gold, GUILayout.MaxWidth(64));
        EditorGUILayout.EndHorizontal();
        #endregion
    }

    public void AudioInfo()
    {
        // if you need to add an additional audio clip- add a new entry to the enum field CreateNewCharacter.CharacterAudio.ClipName
        // the editor will pupulate the field automatically
        CreateNewCharacter Toon = (CreateNewCharacter)target;

        #region Audio
        EditorGUILayout.Space();
        EditorGUILayout.HelpBox("Sounds", MessageType.None);

        #region Count how many ClipNames are currently set in CreateNewCharacter.CharacterAudio
        int Count = 0;
        foreach (CreateNewCharacter.CharacterAudio.ClipName clip in System.Enum.GetValues(typeof(CreateNewCharacter.CharacterAudio.ClipName)))
        {
            Count++;
        }
        #endregion

        #region if that number is different than what AudioClips.Count shows, wipe AudioClips.Count and re-populate
        if (Toon.AudioClips.Count != Count)
        {
            Toon.AudioClips.Clear();
            foreach (CreateNewCharacter.CharacterAudio.ClipName clip in System.Enum.GetValues(typeof(CreateNewCharacter.CharacterAudio.ClipName)))
            {
                CreateNewCharacter.CharacterAudio A = new CreateNewCharacter.CharacterAudio();
                A.Name = clip;
                A.Clip = new AudioClip();
                Toon.AudioClips.Add(A);
            }
        }
        #endregion

        #region for i loop to draw the inspector shit
        for (int i = 0; i < Toon.AudioClips.Count; i++)
        {
            string Name = Toon.AudioClips[i].Name.ToString();
            Toon.AudioClips[i].Clip = (AudioClip)EditorGUILayout.ObjectField(Name, Toon.AudioClips[i].Clip, typeof(AudioClip), true);
        }

        #endregion
        #endregion
    }

    public void AbilityInfo()
    {
        CreateNewCharacter Toon = (CreateNewCharacter)target;

        #region Abilities
        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.HelpBox("Base Moves", MessageType.None);
        EditorGUILayout.EndHorizontal();

        #region Add an ability to Toon.Skills if there are none there to appease the elder gods
        if (Toon.Skills.Count == 0)
        {
            SpellObject s = new SpellObject();
            Toon.Skills.Add(s);
        }
        #endregion

        #region For i loop to draw the inspector shit
        for (int i = 0; i < Toon.Skills.Count; i++)
        {
            Toon.Skills[i] = (SpellObject)EditorGUILayout.ObjectField(Toon.Skills[i], typeof(SpellObject), false);
        }
        #endregion

        #region "Add Spell" Button
        if (GUILayout.Button("Add Spell"))
        {
            SpellObject s = new SpellObject();
            Toon.Skills.Add(s);
        }
        #endregion

        #region "Remove Spell" Button
        // the length check is there so that you dont subtract 1 from a 0 length list
        //and end up with a -1 index and lock up the editor
        if (GUILayout.Button("Remove Spell") && Toon.Skills.Count > 1)
        {
            int index = Toon.Skills.Count - 1;
            Toon.Skills.RemoveAt(index);
        }
        #endregion

        #endregion
    }


}