﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System;

[CustomEditor(typeof(CreateSpellBook))]
public class SpellBookEditor : Editor {

    public string[] Folders = new string[]
    {
        "Spells",
        "Toons",
        "Attacks/ResourceTypes",
        "Attacks/DamageTypes",
        "Attacks/RangedBehaviors"
    };

    public Type[] types = new Type[]
    {
        typeof(SpellObject),
        typeof(CreateNewCharacter),
        typeof(CreateNewResource),
        typeof(CreateNewDamageType),
        typeof(CreateSplineProfile)
    };

    public override void OnInspectorGUI()
    {
        foreach (Type t in typeof(CreateSpellBook).Assembly.GetExportedTypes())
        {
            for(int i = 0; i < types.Length; i++)
            {
                if (types[i] == t)
                {
                    DrawTitle(t);

                    Array myArray = Array.CreateInstance(t, 0);
                    myArray = Resources.LoadAll(Folders[i]).OrderBy(x => x.name).ToArray();

                    for (int j = 0; j < myArray.GetLength(0); j++)
                    {
                        string a = EditorGUILayout.TextField(myArray.GetValue(j).ToString(), GUILayout.MaxWidth(256));
                    }
                }
            }
            
        }

        #region GUI Shit
        //base.OnInspectorGUI();
        #endregion
    }

    void DrawTitle(Type t)
    {
        EditorGUILayout.Space();
        EditorGUILayout.HelpBox(t.ToString(), MessageType.None);
    }


}
