﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ActiveEffects))]
public class ActiveEffectsEditor : Editor
{
    public override void OnInspectorGUI()
    {
        ActiveEffects A = (ActiveEffects)target;

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Effect Name");
        EditorGUILayout.LabelField("Remaining Duration");
        EditorGUILayout.EndHorizontal();

        for (int i = 0; i < A.EffectList.Count; i++)
        {

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(A.EffectList[i].SpellName);
            EditorGUILayout.FloatField(A.EffectList[i].SpellTimer.EndTime - Time.time);
            EditorGUILayout.EndHorizontal();
        }

        base.OnInspectorGUI();

        EditorUtility.SetDirty(A);
    }
}
