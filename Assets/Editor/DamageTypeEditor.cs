﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(CreateNewDamageType))]
public class DamageTypeEditor : Editor {

    public override void OnInspectorGUI()
    {
        CreateNewDamageType D = (CreateNewDamageType)target;

        DrawElements();

        #region GUI Settings
        //base.OnInspectorGUI();
        if (GUI.changed)
        {
            EditorUtility.SetDirty(D);
        }
        #endregion
    }

    void DrawElements()
    {
        CreateNewDamageType D = (CreateNewDamageType)target;

        #region Draw Weaknesses
        #region Draw Header
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("Weakness");
        EditorGUILayout.LabelField("Amount");

        EditorGUILayout.EndHorizontal();
        #endregion

        #region Draw elements
        for (int i = 0; i < D.Weak.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            D.Weak[i] = (CreateNewDamageType)EditorGUILayout.ObjectField(D.Weak[i], typeof(CreateNewDamageType), true);
            D.WeakAmnt[i] = EditorGUILayout.FloatField(D.WeakAmnt[i]);
            EditorGUILayout.EndHorizontal();
        }
        #endregion

        #region Draw Buttons
        EditorGUILayout.BeginHorizontal();
        if(GUILayout.Button("Add Weakness"))
        {
            D.Weak.Add(new CreateNewDamageType());
            D.WeakAmnt.Add(0);
        }

        if(GUILayout.Button("Remove Weakness"))
        {
            if(D.Weak.Count > 0 && D.WeakAmnt.Count > 0)
            {
                D.Weak.RemoveAt(D.Weak.Count - 1);
                D.WeakAmnt.RemoveAt(D.WeakAmnt.Count - 1);
            }
        }
        EditorGUILayout.EndHorizontal();
        #endregion
        #endregion

        #region Draw Strengths
        #region Draw Header
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("Resist");
        EditorGUILayout.LabelField("Amount");

        EditorGUILayout.EndHorizontal();
        #endregion

        #region Draw elements
        for (int i = 0; i < D.Resist.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            D.Resist[i] = (CreateNewDamageType)EditorGUILayout.ObjectField(D.Resist[i], typeof(CreateNewDamageType), true);
            D.ResistAmnt[i] = EditorGUILayout.FloatField(D.ResistAmnt[i]);
            EditorGUILayout.EndHorizontal();
        }
        #endregion

        #region Draw Buttons
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Add Resist"))
        {
            D.Resist.Add(new CreateNewDamageType());
            D.ResistAmnt.Add(0);
        }

        if (GUILayout.Button("Remove Resist"))
        {
            if (D.Resist.Count > 0 && D.ResistAmnt.Count > 0)
            {
                D.Resist.RemoveAt(D.Resist.Count - 1);
                D.ResistAmnt.RemoveAt(D.ResistAmnt.Count - 1);
            }
        }
        EditorGUILayout.EndHorizontal();
        #endregion
        #endregion

    }
}