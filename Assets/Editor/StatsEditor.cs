﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(Stats))]
public class StatsEditor : Editor {

    #region Variables
    public bool ShowStats;
    public bool AbilityFoldout;
    public bool[] CollectionFoldouts = new bool[20];
    public bool[] StatFoldouts = new bool[20];
    public bool ShowHealthBar;
    public bool ShowResourceBar;

    float Totalwidth;
    float small;
    float mid;
    float big;
    float space;
    float leftpad;

    float smallt;
    float midt;
    float bigt;
    float spacet;
    float leftpadt;
    #endregion

    public override void OnInspectorGUI()
    {
        Stats S = (Stats)target;

        #region Method Invoke Order
        Intiaialize();
        Profile();
        Team();
        ShowAttributes();
        Collection();
        Abilities();
        #endregion

        #region GUI settings
        //base.OnInspectorGUI();
        if (GUI.changed)
        {
            EditorUtility.SetDirty(S);
        }
        #endregion
    }

    void Intiaialize()
    {
        Stats S = (Stats)target;

        #region Populate Collection if needed
        if(S.Collection == null)
        {
            S.Collection = new Stats.Collections();
        }
        #endregion

        #region Repopulate Attributes if change is detected in Attributes.StatName
        if(S.Attributes.Count < S.Toon_Profile.Stats.Count)
        {
            for (int i = 0; i < S.Toon_Profile.Stats.Count; i++)
            {
                S.Attributes.Add(S.Toon_Profile.Stats[i].Stat.ToString(), S.Toon_Profile.Stats[i].Value);
            }
        }
        #endregion

        #region GUI settings

        //Totalwidth = EditorGUILayout.FloatField("inspector width", Screen.width);
        //smallt = EditorGUILayout.Slider("small elements", smallt, 0, 1);
        //midt = EditorGUILayout.Slider("mid elements", midt, 0, 1);
        //bigt = EditorGUILayout.Slider("big elements", bigt, 0, 1);
        //spacet = EditorGUILayout.Slider("spaces", spacet, 0, 1);
        //leftpadt = EditorGUILayout.Slider("left padding", leftpadt, 0, 1);

        Totalwidth = Screen.width;
        smallt = 0.089f;
        midt = 0.161f;
        bigt = 0.292f;
        spacet = 0;
        leftpadt = 0.026f;

        small = Totalwidth * smallt;
        mid = Totalwidth * midt;
        big = Totalwidth * bigt;
        space = Totalwidth * spacet;
        leftpad = Totalwidth * leftpadt;
        #endregion
    }

    void Profile()
    {
        Stats S = (Stats)target;

        #region Populate the Character Profile
        S.Toon_Profile = (CreateNewCharacter)EditorGUILayout.ObjectField("Character Profile", S.Toon_Profile, typeof(CreateNewCharacter), true);
        EditorGUILayout.Space();
        #endregion 
    }

    void Team()
    {
        Stats S = (Stats)target;
        S.Faction = (CreateNewFaction)EditorGUILayout.ObjectField("Faction", S.Faction, typeof(CreateNewFaction), true);
    }

    void ShowAttributes()
    {
        Stats S = (Stats)target;

        #region Grab the attributes out of the dictionary in Stats script. Attribute names are the Keys and the Values Class in the Value
        // the EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(32)); line is used for indentation. I'm not totally thrilled with the look of it
        // so feel free to alter/remove

        EditorGUILayout.HelpBox("Attributes", MessageType.None);
        Healthbar();
        ResourceBar();
        ShowStats = EditorGUILayout.Foldout(ShowStats, "Show Stats");
        if (ShowStats)
        {
            int Count = 0;
            foreach (System.Collections.Generic.KeyValuePair<string, Attributes.Values> key in S.Attributes)
            {
                EditorGUILayout.HelpBox(key.Key.ToString(), MessageType.None);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(32));
                StatFoldouts[Count] = EditorGUILayout.Foldout(StatFoldouts[Count], "Show " + key.Key.ToString());
                EditorGUILayout.EndHorizontal();
                if (StatFoldouts[Count])
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(32));
                    EditorGUILayout.IntField("Max Value", S.Attributes[key.Key].Max);
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(32));
                    EditorGUILayout.IntField("Curren Value", S.Attributes[key.Key].Current);
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(32));
                    EditorGUILayout.IntField("Base Value", S.Attributes[key.Key].Base);
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(32));
                    EditorGUILayout.IntField("Regen Rate", S.Attributes[key.Key].Regen);
                    EditorGUILayout.EndHorizontal();
                }
                Count++;
            }
        }
        #endregion 

    }

    void Collection()
    {
        Stats S = (Stats)target;

        #region Grab the data hanging out in the Stats.Collections class
        EditorGUILayout.HelpBox("Show Collections", MessageType.None);

        #region Experience
        CollectionFoldouts[0] = EditorGUILayout.Foldout(CollectionFoldouts[0], "Experience");
        if (CollectionFoldouts[0])
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.IntField("Experience", S.Collection.Experience);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.IntField("Experience To Next Level", S.Collection.ExperienceToLevel);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.IntField("Experience Granted On Kill", S.Collection.ExperienceOnKill);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.FloatField("Experience Multiplier", S.Collection.ExperienceMultiplier);
            EditorGUILayout.EndHorizontal();

        }
        #endregion

        #region Gold
        CollectionFoldouts[1] = EditorGUILayout.Foldout(CollectionFoldouts[1], "Gold");
        if (CollectionFoldouts[1])
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.IntField("Gold", S.Collection.Gold);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.IntField("Gold on Kill", S.Collection.GoldOnKill);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.IntField("Gold Multiplier", S.Collection.GoldMultiplier);
            EditorGUILayout.EndHorizontal();

        }
        #endregion

        #region K/D
        CollectionFoldouts[2] = EditorGUILayout.Foldout(CollectionFoldouts[2], "Kills/Deaths");
        if (CollectionFoldouts[2])
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.IntField("Kills", S.Collection.Kills);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.IntField("Longest Kill Streak", S.Collection.KillStreak);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.IntField("Deaths", S.Collection.Deaths);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.IntField("Longest Death Streak", S.Collection.DeathStreak);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.HelpBox("Creatres Killed / Times Killed", MessageType.None);
            EditorGUILayout.EndHorizontal();

            for (int i = 0; i < S.Collection.CreaturesKilled.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
                EditorGUILayout.LabelField(S.Collection.CreaturesKilled[i].Toon_Name);
                EditorGUILayout.IntField(S.Collection.TimesKilled[i]);
                EditorGUILayout.EndHorizontal();
            }


        }
        #endregion

        #region Damage and Healing
        CollectionFoldouts[3] = EditorGUILayout.Foldout(CollectionFoldouts[3], "Damage and Healing");
        if (CollectionFoldouts[3])
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.IntField("Damage Taken", S.Collection.DamageTaken);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.IntField("Damage Dealt", S.Collection.DamageDealt);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.IntField("Healing Taken", S.Collection.HealingTaken);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" ", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.IntField("Healing Done", S.Collection.HealingDone);
            EditorGUILayout.EndHorizontal();

        }
        #endregion

        #endregion
    }

    void Abilities()
    {
        Stats S = (Stats)target;

        #region Abilities
        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.HelpBox("Abilities", MessageType.None);
        EditorGUILayout.EndHorizontal();

        AbilityFoldout = EditorGUILayout.Foldout(AbilityFoldout, "Show Abilities");
        if (AbilityFoldout)
        {
            #region Add a ability to the Skills list if none are currently there
            if (S.Toon_Profile.Skills.Count == 0)
            {
                SpellObject s = new SpellObject();
                S.Toon_Profile.Skills.Add(s);
            }
            #endregion

            #region GUI layout editing options
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("", GUILayout.MaxWidth(leftpad));
            EditorGUILayout.LabelField("Ability ", GUILayout.MaxWidth(big));
            EditorGUILayout.LabelField("", GUILayout.MaxWidth(space));
            EditorGUILayout.LabelField("Ready", GUILayout.MaxWidth(small));
            EditorGUILayout.LabelField("", GUILayout.MaxWidth(space));
            EditorGUILayout.LabelField("CD Remaining", GUILayout.MaxWidth(mid));
            EditorGUILayout.EndHorizontal();
           

            #endregion 

            #region For i loop to populate the spells
            for (int i = 0; i < S.Toon_Profile.Skills.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("", GUILayout.MaxWidth(leftpad));
                S.Toon_Profile.Skills[i] = (SpellObject)EditorGUILayout.ObjectField(S.Toon_Profile.Skills[i], typeof(SpellObject), false, GUILayout.MaxWidth(big));
                EditorGUILayout.LabelField("", GUILayout.MaxWidth(space));
                EditorGUILayout.Toggle(S.Toon_Profile.Skills[i].Ready, GUILayout.MaxWidth(small));
                EditorGUILayout.LabelField("", GUILayout.MaxWidth(space));
                EditorGUILayout.FloatField(S.Toon_Profile.Skills[i].CD_Remaining, GUILayout.MaxWidth(mid));
                EditorGUILayout.EndHorizontal();
            }
            #endregion

            #region Buttons
            EditorGUILayout.BeginHorizontal();
            #region "Add Spell" Button
            if (GUILayout.Button("Add Spell"))
            {
                SpellObject s = new SpellObject();
                S.Toon_Profile.Skills.Add(s);
            }
            #endregion

            #region "Remove Spell" Button
            // that length check is there because if you attempt to remove an entry from a list of length 0
            // you will get a -1 index and bug out the editor
            if (GUILayout.Button("Remove Spell") && S.Toon_Profile.Skills.Count > 1)
            {
                int index = S.Toon_Profile.Skills.Count - 1;
                S.Toon_Profile.Skills.RemoveAt(index);
            }
            #endregion
            EditorGUILayout.EndHorizontal();
            #endregion

        }
        #endregion
    }

    void Healthbar()
    {
        Stats S = (Stats)target;

        ShowHealthBar = EditorGUILayout.Foldout(ShowHealthBar, "Show Health Bar");
        if (ShowHealthBar)
        {
            #region Health Bar
            float fraction = 0;
            if (S.Attributes[Attributes.StatName.Health.ToString()].Max > 0)
            {
                fraction = S.Attributes[Attributes.StatName.Health.ToString()].Current / S.Attributes[Attributes.StatName.Health.ToString()].Max;
            }
            else
            {
                fraction = 0;
            }

            string Text = "Health : " + S.Attributes[Attributes.StatName.Health.ToString()].Current + " / " + S.Attributes[Attributes.StatName.Health.ToString()].Max;
            Rect r = EditorGUILayout.BeginVertical();
            EditorGUI.ProgressBar(r, fraction, Text);
            GUILayout.Space(16);
            EditorGUILayout.EndVertical();
            #endregion
        }
    }

    void ResourceBar()
    {
        Stats S = (Stats)target;

        ShowResourceBar = EditorGUILayout.Foldout(ShowResourceBar, "Show Rersource Bar");
        if (ShowResourceBar)
        {
            #region Resource Bar
            float fraction = 0;
            if (S.Attributes[Attributes.StatName.Resources.ToString()].Max > 0)
            {
                fraction = S.Attributes[Attributes.StatName.Resources.ToString()].Current / S.Attributes[Attributes.StatName.Resources.ToString()].Max;
            }
            else
            {
                fraction = 0;
            }

            string Text = S.Toon_Profile.ResourceType.Name + ": " + S.Attributes[Attributes.StatName.Resources.ToString()].Current + " / " + S.Attributes[Attributes.StatName.Resources.ToString()].Max;
            Rect r = EditorGUILayout.BeginVertical();
            EditorGUI.ProgressBar(r, fraction, Text);
            GUILayout.Space(16);
            EditorGUILayout.EndVertical();
            #endregion
        }
    }

}
